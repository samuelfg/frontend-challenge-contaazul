// jshint ignore: start
'use strict';

var pkg = require('../package');
var path = require('path');
var argv = require('yargs').argv;

exports.NAME = pkg.name;
exports.VERSION = pkg.version;
exports.NOW = Math.round(Date.now()/1000);


exports.Path = {
  BUILD:    'build/',
  SOURCE:   'src/'
};
