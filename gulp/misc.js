// jshint ignore: start
'use strict';

var gulp = require('gulp');
var watch = require('gulp-watch');
var fs = require('fs');

exports.watch = function () {
  gulp.watch('./code/**/*.{js,json,htm,html}', ['build']);
};

exports.getPkgInfo = function () {
  return JSON.parse(fs.readFileSync('./package.json', 'utf8'));
};

