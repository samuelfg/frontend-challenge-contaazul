// jshint ignore: start
'use strict';

var script = require('./script');
var config = require('./config');
var gulp = require('gulp');
var merge = require('merge-stream');
var del = require('del');
var moment = require('moment');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var path = require('path');
var zip = require('gulp-zip');

module.exports = exports = function () {
  return merge(
    script.build(),
    script.copyViews(),
    script.copyStyles(),
    script.copyIndex(),
    script.copyScripts(),
    script.copyImages(),
    script.copyData()
  );
};

module.exports.watch = function () {
	del(['./build'], done);
	script.watch();
};
