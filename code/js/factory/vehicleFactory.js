Module.factory('VehiclesFactory', [
	'$http',
	'UriService',
	'$q',
	'Constants',
	'UtilsService',
	function (
		$http,
		Uri,
		$q,
		Constants,
		Utils
	) {
	'use strict';
	var Vehicles = {};

	
	Vehicles.get = function() {
		Utils.log('Vehicles.get');
		return _get().then(function (data) {
			Utils.log(data);
			return data;
		});
	};	
	

	//*** private methods ****************************************************


	
	function _get(url) {
		console.log(Uri.getVehicles);
		return $http.get(Uri.getVehicles)
			.then(function (response) {
				return {
					success: true,
					response: response.data
				};
			})
			.catch(function (error) {		
				throw error.statusText;
			});
	}
	
	return Vehicles;

}]);