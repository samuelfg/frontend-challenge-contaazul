//service to group application constants and configurations
Module.service('Constants', function () {
	'use strict';
	
	//configs
	this.DEBUG_MODE = [[debug-mode]];
	this.PAGE_SIZE = 5;

});