//service used to group url that application need
Module.service('UriService', function () {
	'use strict';
	//get all
	this.getVehicles = getContextPath() + '/resources/data/vehicles.json';

	/**
	 * get page context path
	 * used to concatenate on service url's
	 * @returns {string} page context path
	 */
	function getContextPath() {
		return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
	}


});