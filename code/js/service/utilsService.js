//service to group utils methods
Module.service('UtilsService', [
	'Constants',
	function (
	Constants
	) {
	'use strict';
	
	/**
	 * log object if it is enabled
	 * @param {Object} object to log
	 */
	this.log = function(obj) {
		if(Constants.DEBUG_MODE) {
			console.log(obj);
		}
	};
	
	/**
	 * Parse form to JSON object
	 */
	this.serializeForm = function(formSelector) {
		var inputs = $(formSelector+' :input');
		var _serialized = {};
		$.each(inputs, function(n, i)
		{
			n = inputs[n];
			_serialized[n.name] = $(n).val();
			
		});
		return _serialized;
	};
	
	this.populateFormVehicle = function(vehicle) {
		var inputs = $("#formVehicle :input");
		
		$.each(inputs, function(input) {
			input = inputs[input];			
			input.value = vehicle[input.name];
		});
		
	};
	
	/**
	 * Generate an alphanumeric unique id
	 */
	this.generateId = function() {
		return Math.random().toString(36).slice(2);
	};
	
	
	this.getPageByList = function(list, page) {
		return list.slice(page*Constants.PAGE_SIZE, (page*Constants.PAGE_SIZE + Constants.PAGE_SIZE));
	};

}]);