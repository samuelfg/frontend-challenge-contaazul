Module.controller('vehiclesController', [
	'$scope',
	'$rootScope',
	'VehiclesFactory',
	'Constants',
	'UtilsService',
	'$timeout',
	function(
		$scope,
		$rootScope,
		Vehicles,
		Constants,
		Utils,
		$timeout
	){
	'use strict';
	
	$scope.vehicles = [];
	$scope.selecteds = [];
	$scope.selectedAll = false;
	$scope.currentPage = 1;
	$scope.totalPages = 0;
	$scope.pictureVisible = false;
	
	/**
	 * Return all vehicles
	 */
	$scope.getVehicles = function() {
		Vehicles.get().then(function (data) {
			$scope.vehicles = data.response;
			$scope.allVehicles = data.response;
			$scope.currentList = $scope.allVehicles;
			$scope.refreshPagination();
		});
	};
	
	/**
	 * Add vehicle object converted by serializeForm() to 
	 * $scope.allVehicles array
	 */
	$scope.saveVehicle = function() {
		
		if(($scope.formError = !$('#formVehicle').get(0).checkValidity())) {
			return;
		}
		
		function getPositionById(id) {
			
			for(var vehicle in $scope.allVehicles) {
				Utils.log($scope.allVehicles[vehicle].id +"=="+ id);
				if($scope.allVehicles[vehicle].id == id) {
					return vehicle;
				}
			}
			return -1;
		}
		
		//make object by form fields
		var vehicle = Utils.serializeForm('#formVehicle');	
		
		var position = getPositionById(vehicle.id);
		Utils.log('Editing vehicle id: ' + vehicle.id + ', position: ' + position);
		
		if(position > -1) {
			$scope.allVehicles[position] = vehicle;
		} else {
			vehicle.id = Utils.generateId();
			vehicle.selected = false;
			$scope.allVehicles.unshift(vehicle);
		}
		$scope.filterVehicle();
		$scope.setSelected();
		$('#vehicleModal').modal('hide');
			
	};
	
	/**
	 * Set or refresh array of selecteds vehicles.
	 * @param {vehicle} vehicle object
	 * @param {event} event from view
	 */
	$scope.setSelected = function(vehicle, event) {
		if(vehicle && event && event.currentTarget) {
			vehicle.selected = event.currentTarget.checked;
		}
		$scope.selecteds = jQuery.grep($scope.allVehicles, function(vehicle) {
		  return vehicle.selected;
		});
	};
	
	/**
	 * Remove all vehicles that have selected proeprty as true.
	 */	
	$scope.deleteVehicles = function() {
	
		if (!$('#deleteConfirm').is(':visible')) {
			$("#deleteConfirm").modal('show');
		} else {	
			$scope.allVehicles = jQuery.grep($scope.allVehicles, function(vehicle) {
			  return !vehicle.selected;
			});
			$("#deleteConfirm").modal('hide');
		}
		$scope.currentList = $scope.allVehicles;
		$scope.filterVehicle();
		$scope.setSelected();
	};
	
	/**
	 * Reset new vehicle form.
	 */	
	$scope.newVehicle = function() {
		$('#formVehicle').get(0).reset();
		$('#inputId').val('');
		$scope.formError = false;		
	};
	
	$scope.editVehicle = function(vehicle) {
		$scope.formError = false;
		if($scope.selecteds.length > 1 && !vehicle) {
			$("#selectedVehicles").modal('show');
		} else {
			vehicle = vehicle ? vehicle : $scope.selecteds[0];
			Utils.populateFormVehicle(vehicle);
			$("#selectedVehicles").modal('hide');
			$('#vehicleModal').modal('show');
		}
	};
	
	/**
	 * Works as backup from original vehicles array
	 */	
	$scope.restoreVehicle = function() {
		$scope.vehicles = $scope.allVehicles;
	};
	
	/**
	 * Filter cloned vehicles array
	 * It filters by modelo, placa, marca and combustivel params
	 */
	$scope.filterVehicle = function(searchString) {
	
		searchString = (searchString || ($('#inputSearch').val() || '')).toLowerCase();
		$scope.restoreVehicle();	
		if(searchString.length > 0) {
			$scope.vehicles = jQuery.grep($scope.vehicles, function(vehicle) {
			  return ((vehicle.placa && vehicle.placa.toLowerCase().indexOf(searchString) > -1) ||
					(vehicle.modelo && vehicle.modelo.toLowerCase().indexOf(searchString) > -1) ||
					(vehicle.marca && vehicle.marca.toLowerCase().indexOf(searchString) > -1) ||
					(vehicle.combustivel && vehicle.combustivel.toLowerCase().indexOf(searchString) > -1));
			});	
			$scope.currentList = $scope.vehicles;
		} else { //reset filter			
			$scope.currentList = $scope.allVehicles;
		}
		
		$scope.refreshPagination();
	};
	
	/**
	 * Set all allVehicles objects as true for selected property
	 */
	$scope.checkAll = function(event) {
	
		$scope.selectedAll = !$scope.selectedAll;
		$.each($scope.vehicles, function(i) {
			$scope.vehicles[i].selected = event.currentTarget.checked;
		});
		
		//do to all if filtered
		if($scope.allVehicles) {
			$.each($scope.allVehicles, function(i) {
				$scope.allVehicles[i].selected = event.currentTarget.checked;
			});
		}
		$scope.setSelected();
	};
		
	$scope.refreshPagination = function() {
		$scope.totalPages = Math.ceil(Number($scope.currentList.length/Constants.PAGE_SIZE));
		Utils.log("Page size: " + $scope.totalPages);
		$scope.setPage(0);		
	};
	
	$scope.setPage = function(page) {
		if(page >= $scope.totalPages || page < 0) {
			return;
		}
		$scope.currentPage = page;
		$scope.vehicles = Utils.getPageByList($scope.currentList, page);
	};	
	
	/**
	 * Show vehicle image
	 */
	$scope.toogleModalImage = function(src) {
		var modal = $('.modal-image-bg');
		if($scope.pictureVisible === false) {
			modal.fadeIn(500, function(){
				$('#carImage').attr("src", src);
				$scope.pictureVisible = true;
				$scope.$apply();
				console.log(src);
			});
		} else {
			$scope.pictureVisible = false;
			modal.fadeOut(500);
		}
	};
	
	/**
	 * Append here functions that need to be executed when
	 * initialize it
	 */
	function initialize() {
		Utils.log('initializing...');
		$scope.getVehicles();
		
	}

	initialize();


}]);

